#include <iostream>

using namespace std;

class Node
{
public:
	Node(int value);

	int getValue();
	Node* getLeft();
	Node* getRight();

	void setValue(int value);
	void setLeft(Node* left);
	void setRight(Node* right);

private:
	int value;
	Node* left;
	Node* right;
};

class SplayTree
{
public:
	SplayTree();
	SplayTree(int value);
	~SplayTree();

	int getRootValue();
	void add(int value);
	Node* find(int value);
	void printPreorder();
	void printInorder();
	void printPostorder();
	void remove(int value);
	void splay(int value);

private:
	Node* root;

	void add(int value, Node* node);
	Node* find(int value, Node* node);
	void preorder(Node* node);
	void inorder(Node* node);
	void postorder(Node* node);
	Node* remove(int value, Node* node);
	Node* findSuccessor(Node* node);
	Node* removeSuccessor(Node* node);
	Node* rotateLeft(Node* node);
	Node* rotateRight(Node* node);
	Node* splay(int value, Node* node);
	void free(Node* node);
};

Node::Node(int value) : value(value)
{
	left = nullptr;
	right = nullptr;
}

int Node::getValue()
{
	return value;
}

Node* Node::getLeft()
{
	return left;
}

Node* Node::getRight()
{
	return right;
}

void Node::setValue(int value)
{
	this->value = value;
}

void Node::setLeft(Node* left)
{
	this->left = left;
}

void Node::setRight(Node* right)
{
	this->right = right;
}

SplayTree::SplayTree()
{
	root = nullptr;
}

SplayTree::SplayTree(int value)
{
	root = new Node(value);
}

SplayTree::~SplayTree()
{
	free(root);
}

int SplayTree::getRootValue()
{
	return root->getValue();
}

void SplayTree::add(int value)
{
	add(value, root);
	splay(value);
}

Node* SplayTree::find(int value)
{
	root = splay(value, root);

	if (getRootValue() == value)
	{
		cout << "Znaleziono " << getRootValue() << endl;
		return root;
	}
	else
		return nullptr;
}

void SplayTree::printPreorder()
{
	cout << "Preorder:" << endl;
	preorder(root);
	cout << endl;
}

void SplayTree::printInorder()
{
	cout << "Inorder:" << endl;
	inorder(root);
	cout << endl;
}

void SplayTree::printPostorder()
{
	cout << "Postorder:" << endl;
	postorder(root);
	cout << endl;
}

void SplayTree::remove(int value)
{
	root = splay(value, root);

	if (getRootValue() == value)
		root = remove(value, root);
}

void SplayTree::splay(int value)
{
	cout << "Splay: " << value << endl;
	root = splay(value, root);
}

void SplayTree::add(int value, Node* node)
{
	if (root == nullptr)
		root = new Node(value);
	else
	{
		if (node->getValue() == value)
			cout << "Element o wartosci " << value << " juz istnieje!\n";
		else if (node->getValue() > value)
		{
			if (node->getLeft() == nullptr)
				node->setLeft(new Node(value));
			else
				add(value, node->getLeft());
		}
		else
		{
			if (node->getRight() == nullptr)
				node->setRight(new Node(value));
			else
				add(value, node->getRight());
		}
	}
}

Node* SplayTree::find(int value, Node* node)
{
	if (node == nullptr)
		cout << "Element o wartosci " << value << " nie istnieje!\n";
	else
	{
		if (node->getValue() == value)
			cout << "Znaleziono: " << node->getValue() << endl;
		else if (node->getValue() > value)
			node = find(value, node->getLeft());
		else
			node = find(value, node->getRight());
	}

	return node;
}

void SplayTree::preorder(Node* node)
{
	if (node != nullptr)
	{
		cout << node->getValue() << ", ";
		preorder(node->getLeft());
		preorder(node->getRight());
	}
}

void SplayTree::inorder(Node* node)
{
	if (node != nullptr)
	{
		inorder(node->getLeft());
		cout << node->getValue() << ", ";
		inorder(node->getRight());
	}
}

void SplayTree::postorder(Node* node)
{
	if (node != nullptr)
	{
		postorder(node->getLeft());
		postorder(node->getRight());
		cout << node->getValue() << ", ";
	}
}

Node* SplayTree::remove(int value, Node* node)
{
	if (node == nullptr)
	{
		cout << "Element o wartosci " << value << " nie istnieje!\n";
		return node;
	}
	else if (node->getValue() > value)
		node->setLeft(remove(value, node->getLeft()));
	else if (node->getValue() < value)
		node->setRight(remove(value, node->getRight()));
	else
	{
		if (node->getLeft() == nullptr && node->getRight() == nullptr)
		{
			delete node;
			node = nullptr;
		}
		else if (node->getLeft() == nullptr)
		{
			Node* toRemove = node;
			node = node->getRight();
			delete toRemove;
		}
		else if (node->getRight() == nullptr)
		{
			Node* toRemove = node;
			node = node->getLeft();
			delete toRemove;
		}
		else
		{
			Node* toRemove = node;
			node = findSuccessor(toRemove->getRight());
			node->setRight(removeSuccessor(toRemove->getRight()));
			node->setLeft(toRemove->getLeft());
			delete toRemove;
		}
		cout << "Usunieto " << value << endl;
	}

	return node;
}

Node* SplayTree::findSuccessor(Node* node)
{
	if (node->getLeft() == nullptr)
		return node;

	return findSuccessor(node->getLeft());
}

Node* SplayTree::removeSuccessor(Node* node)
{
	if (node->getLeft() == nullptr)
		return node->getRight();

	node->setLeft(removeSuccessor(node->getLeft()));

	return node;
}

Node* SplayTree::rotateLeft(Node* node)
{
	Node* child = node->getRight();

	node->setRight(child->getLeft());
	child->setLeft(node);

	return child;
}

Node* SplayTree::rotateRight(Node* node)
{
	Node* child = node->getLeft();

	node->setLeft(child->getRight());
	child->setRight(node);

	return child;
}

Node* SplayTree::splay(int value, Node* node)
{
	if (node == nullptr)
	{
		cout << "Nie znaleziono: " << value << endl;
		return node;
	}

	if (node->getValue() == value)
		return node;
	else if (node->getValue() > value)
	{
		if (node->getLeft() == nullptr)
		{
			cout << "Nie znaleziono: " << value << endl;
			return node;
		}

		if (node->getLeft()->getValue() > value)	// (zig) - zig
		{
			node->getLeft()->setLeft(splay(value, node->getLeft()->getLeft()));
			node = rotateRight(node);

		}
		else if (node->getLeft()->getValue() < value)	// (zig) - zag
		{
			node->getLeft()->setRight(splay(value, node->getLeft()->getRight()));

			if (node->getLeft()->getRight() != nullptr)
				node->setLeft(rotateLeft(node->getLeft()));
		}

		if (node->getLeft() != nullptr)
			node = rotateRight(node);	// zig
	}
	else
	{
		if (node->getRight() == nullptr)
		{
			cout << "Nie znaleziono: " << value << endl;
			return node;
		}

		if (node->getRight()->getValue() < value)	// (zig) - zig
		{
			node->getRight()->setRight(splay(value, node->getRight()->getRight()));
			node = rotateLeft(node);

		}
		else if (node->getRight()->getValue() > value)	// (zig) - zag
		{
			node->getRight()->setLeft(splay(value, node->getRight()->getLeft()));

			if (node->getRight()->getLeft() != nullptr)
				node->setRight(rotateRight(node->getRight()));
		}

		if (node->getRight() != nullptr)
			node = rotateLeft(node);	// zig
	}

	return node;
}

void SplayTree::free(Node* node)
{
	if (node != nullptr)
	{
		free(node->getLeft());
		free(node->getRight());
		delete node;
	}
}


int main()
{
	const int size = 10;
	const int elements[size] = { 25, 30, 22, 20, 60, 32, 75, 65, 70, 45 };

	SplayTree* tree = new SplayTree(50);
	for (int i = 0; i < size; i++)
	{
		tree->add(elements[i]);
	}

	tree->printPreorder();

	tree->splay(32);
	cout << "Pierwszy element: " << tree->getRootValue() << endl;

	tree->splay(50);
	cout << "Pierwszy element: " << tree->getRootValue() << endl;

	tree->splay(200);
	cout << "Pierwszy element: " << tree->getRootValue() << endl;

	tree->find(30);
	cout << "Pierwszy element: " << tree->getRootValue() << endl;

	tree->remove(60);

	tree->printPreorder();

	delete tree;

    system("pause");
	return 0;
}
